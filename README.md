# Community Cage Reverse DNS

This repository holds the reverse zone for the Red Hat Community Cage IP range that is then published using [DNS4Tenants](https://osci.io/offers/dns4tenants/). Housed tenants are able to update their reverse by themselves.

## Workflow for Tenants

- fork the repository, branch, add changes and create a PR: topic commits are recommended for readability, PRs with multiple commits are supported
- wait for the CI to complete
- fix things if needed until the CI is green
- RH employees and trusted community members: merge the changes _by yourself_ (you do not need to wait for us)
  if the UI does not allow you to merge after a green CI then something is misconfigured, please contact @osci
- other contributors: ping @osci and wait for us to review your changes

## Workflow for Admins

Allow new members to modify the DNS: go into the `Manage`->`Members` menu for this repository and click "`Invite Members` or `Invite Group` (in this case the tenant must create a subgroup with trusted persons):

- give `Developer` role to RH employees and trusted community members
- give `Reporter` role to other contributors
